#include "DataWidget.h"
#include "ui_DataWidget.h"
#include "ModulationTest.h"

DataWidget::DataWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::DataWidget)
{
  ui->setupUi(this);
  on_isAutoTransmit_clicked();
}

DataWidget::~DataWidget()
{
  delete ui;
}

bool DataWidget::isAutoTransmit()
{
  return ui->isAutoTransmit->isChecked();
}

bool DataWidget::isShift()
{
  if (isAutoTransmit()) return false;
  return ui->isShift->isChecked();
}

bool DataWidget::isRandShift()
{
  return ui->isRandShift->isChecked();
}

int DataWidget::Shift()
{
  return ui->Shift->value();
}

QString DataWidget::SenderName()
{
  return ui->SenderName->text();
}

QString DataWidget::FileName()
{
  return ui->FileName->text();
}

int DataWidget::DataCount()
{
  return ui->DataCount->value();
}

NAVDAT_TIS_qamMode_t DataWidget::tisQAM()
{
  switch(ui->tisQam->currentIndex())
  {
  default:
  case 0:
    return NAVDAT_TIS_qam4;
  case 1:
    return NAVDAT_TIS_qam16;
  }
}

NAVDAT_DS_qamMode_t DataWidget::dsQAM()
{
  switch(ui->dsQam->currentIndex())
  {
  default:
  case 0:
    return NAVDAT_DS_qam4;
  case 1:
    return NAVDAT_DS_qam16;
  case 2:
    return NAVDAT_DS_qam64;
  }
}

double DataWidget::SiganalGain_dB()
{
  return ui->Gain->value();
}

double DataWidget::NoiseLevel_dB()
{
  return ui->SNR->value();
}

bool DataWidget::isSyncWindowOffset()
{
  return ui->isSyncWindowOffset->isChecked();
}

int DataWidget::SyncWindowOffset()
{
  return ui->syncWindowOffset->value();
}

void DataWidget::setSymbolIdx(quint32 idx)
{
  ui->StartIdx->setText(QString::number(idx));
}

void DataWidget::setSenderName2(QString Name)
{
  ui->SenderName2->setText(Name);
}

void DataWidget::setFileName2(QString Name)
{
  ui->FileName2->setText(Name);
}

void DataWidget::setDataCount2(int Count)
{
  ui->DataCount2->setText(QString::number(Count));
}

void DataWidget::setTisQAM(NAVDAT_TIS_qamMode_t mode)
{
  switch(mode)
  {
  default:
    ui->tisQam2->setCurrentIndex(0);
    break;
  case NAVDAT_TIS_qam4:
    ui->tisQam2->setCurrentIndex(1);
    break;
  case NAVDAT_TIS_qam16:
    ui->tisQam2->setCurrentIndex(2);
    break;
  }
}

void DataWidget::setDsQAM(NAVDAT_DS_qamMode_t mode)
{
  switch(mode)
  {
  default:
    ui->dsQam2->setCurrentIndex(0);
    break;
  case NAVDAT_DS_qam4:
    ui->dsQam2->setCurrentIndex(1);
    break;
  case NAVDAT_DS_qam16:
    ui->dsQam2->setCurrentIndex(2);
    break;
  case NAVDAT_DS_qam64:
    ui->dsQam2->setCurrentIndex(3);
    break;
  }
}

void DataWidget::setErrorsCounts(NAVDAT_DataCounter_t* qamErr, NAVDAT_DataCounter_t* frameErr)
{
  ui->errMISqam->setText(QString::number(qamErr->mis));
  ui->errTISqam->setText(QString::number(qamErr->tis));
  ui->errDSqam->setText(QString::number(qamErr->ds));

  ui->errMISframe->setText(QString::number(frameErr->mis));
  ui->errTISframe->setText(QString::number(frameErr->tis));
  ui->errDSframe->setText(QString::number(frameErr->ds));
}

void DataWidget::on_Transmit_clicked()
{
  ModulationTest* pTest = (ModulationTest*)Test;
  if (!pTest->isStarted())
    pTest->start(false);

  ui->SenderName2->clear();
  ui->FileName2->clear();
  ui->DataCount2->clear();

  ui->errMISqam->clear();
  ui->errTISqam->clear();
  ui->errDSqam->clear();
  ui->errMISframe->clear();
  ui->errTISframe->clear();
  ui->errDSframe->clear();

  pTest->transmit();
}

void DataWidget::on_isAutoTransmit_clicked()
{
  ui->Transmit->setEnabled(!ui->isAutoTransmit->isChecked());
  ui->isShift->setEnabled(!ui->isAutoTransmit->isChecked());
}
